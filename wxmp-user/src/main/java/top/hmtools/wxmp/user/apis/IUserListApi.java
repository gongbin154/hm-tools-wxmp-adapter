package top.hmtools.wxmp.user.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.user.model.UserListParam;
import top.hmtools.wxmp.user.model.UserListResult;

@WxmpMapper
public interface IUserListApi {

	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/user/get")
	public UserListResult getUserList(UserListParam userListParam);
}
