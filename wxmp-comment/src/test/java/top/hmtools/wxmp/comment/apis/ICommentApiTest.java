package top.hmtools.wxmp.comment.apis;

import static org.junit.Assert.*;

import org.junit.Test;

import top.hmtools.wxmp.comment.BaseTest;
import top.hmtools.wxmp.comment.models.OpenCommentParam;
import top.hmtools.wxmp.core.model.ErrcodeBean;

public class ICommentApiTest extends BaseTest{
	
	private ICommentApi commentApi;

	/**
	 * FIXME 需要先测试 消息管理组件--群发消息
	 */
	@Test
	public void testOpenComment() {
		OpenCommentParam openCommentParam = new OpenCommentParam();
		openCommentParam.setMsg_data_id(null);
		ErrcodeBean openComment = this.commentApi.openComment(openCommentParam);
		this.printFormatedJson("2.1 打开已群发文章评论（新增接口）", openComment);
	}

	@Test
	public void testCloseComment() {
		fail("Not yet implemented");
	}

	@Test
	public void testListComment() {
		fail("Not yet implemented");
	}

	@Test
	public void testMarkelectComment() {
		fail("Not yet implemented");
	}

	@Test
	public void testUnmarkelectComment() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteComment() {
		fail("Not yet implemented");
	}

	@Test
	public void testReplyComment() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteReplyComment() {
		fail("Not yet implemented");
	}

	@Override
	public void initSub() {
		this.commentApi = this.wxmpSession.getMapper(ICommentApi.class);
	}

}
