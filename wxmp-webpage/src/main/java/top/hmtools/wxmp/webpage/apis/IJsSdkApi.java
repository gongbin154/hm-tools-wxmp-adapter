package top.hmtools.wxmp.webpage.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.webpage.jsSdk.JsapiTicketResult;

@WxmpMapper
public interface IJsSdkApi {

	/**
	 * 获得jsapi_ticket（有效期7200秒，开发者必须在自己的服务全局缓存jsapi_ticket）
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri="/cgi-bin/ticket/getticket?type=jsapi")
	public JsapiTicketResult getTicket();
}
