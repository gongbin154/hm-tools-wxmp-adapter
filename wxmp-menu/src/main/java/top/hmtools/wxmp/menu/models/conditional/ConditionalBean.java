package top.hmtools.wxmp.menu.models.conditional;

/**
 * 创建/删除 个性自定义菜单目录请求微信服务侧成功时反馈的数据描述实体类
 * @author Hybomyth
 *
 */
public class ConditionalBean {

	/**
	 * 个性自定义目录id
	 */
	private String menuid;

	public String getMenuid() {
		return menuid;
	}

	public void setMenuid(String menuid) {
		this.menuid = menuid;
	}

	@Override
	public String toString() {
		return "ConditionalResultBean [menuid=" + menuid + "]";
	}
	
	
}
