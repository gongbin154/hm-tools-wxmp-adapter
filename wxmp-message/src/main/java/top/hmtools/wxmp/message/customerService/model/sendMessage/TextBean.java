package top.hmtools.wxmp.message.customerService.model.sendMessage;

/**
 * 客服接口-发消息-文本消息 的 内容
 * @author hybo
 *
 */
public class TextBean {

	/**
	 * 文本内容
	 */
	private String content;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		return "TextBean [content=" + content + "]";
	}
	
	
}
