package top.hmtools.wxmp.message.group.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class GroupMessageSendStatusResult extends ErrcodeBean {

	private String msg_id;
	
	private String msg_status;

	public String getMsg_id() {
		return msg_id;
	}

	public void setMsg_id(String msg_id) {
		this.msg_id = msg_id;
	}

	public String getMsg_status() {
		return msg_status;
	}

	public void setMsg_status(String msg_status) {
		this.msg_status = msg_status;
	}

	@Override
	public String toString() {
		return "GroupMessageSendStatusResult [msg_id=" + msg_id + ", msg_status=" + msg_status + ", errcode=" + errcode
				+ ", errmsg=" + errmsg + "]";
	}
	
	
}
